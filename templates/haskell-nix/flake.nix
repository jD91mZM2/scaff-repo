{
  description = "My Awesome Haskell Project";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    haskellNix.url = "github:input-output-hk/haskell.nix";
  };

  outputs = { self, nixpkgs, utils, haskellNix }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
      haskellNix' = haskellNix.legacyPackages."${system}";

      project = haskellNix'.haskell-nix.project {
        src = ./.;
      };
    in rec {
      # `nix build`
      packages.{{ dirname }} = project.{{ dirname }}.components.exes.{{ dirname }};
      defaultPackage = packages.{{ dirname }};

      # `nix run`
      apps.{{ dirname }} = utils.lib.mkApp {
        name = "{{ dirname }}";
        drv = packages.{{ dirname }};
      };
      defaultApp = apps.{{ dirname }};

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [];
        nativeBuildInputs = with pkgs; [ stack ];
      };
    });
}
