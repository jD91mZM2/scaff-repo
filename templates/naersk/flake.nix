{% set date = now() | date(format="%Y-%m-%d") -%}
{
  description = "A rust program";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
    mozillapkgs = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, naersk, mozillapkgs }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      mozilla = pkgs.callPackage (mozillapkgs + "/package-set.nix") {};
      rust = (mozilla.rustChannelOf {
        date = "{{ date }}";
        channel = "nightly";
        sha256 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
      }).rust;

      naersk-lib = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };

      nativeBuildInputs = with pkgs; [ /* pkgconfig */ ];
      buildInputs = with pkgs; [ /* openssl */ ];
    in rec {
      # `nix build`
      packages.{{ dirname }} = naersk-lib.buildPackage {
        pname = "{{ dirname }}";
        root = ./.;

        inherit nativeBuildInputs buildInputs;
      };
      defaultPackage = packages.{{ dirname }};

      # `nix run`
      apps.{{ dirname }} = utils.lib.mkApp {
        drv = packages.{{ dirname }};
      };
      defaultApp = apps.{{ dirname }};

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = buildInputs;
        nativeBuildInputs = nativeBuildInputs ++ [ rust ];
      };
    });
}
