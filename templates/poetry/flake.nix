{
  description = "My Awesome Python Project";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      packages.{{ dirname }} = pkgs.poetry2nix.mkPoetryApplication {
        projectDir = ./.;
      };
      defaultPackage = packages.{{ dirname }};

      # `nix run`
      apps.{{ dirname }} = utils.lib.mkApp {
        drv = packages.{{ dirname }};
      };
      defaultApp = apps.{{ dirname }};

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = nixpkgs.lib.singleton (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
        });
        nativeBuildInputs = with pkgs; [ poetry ];
      };
    });
}
